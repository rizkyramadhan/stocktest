provider "aws" {
  region = "ap-southeast-1"
}


#####
# Vpc
#####

module "vpc" {
  source = "../../modules/aws-vpc"

  vpc-location                        = "Singapore"
  namespace                           = "stocktest"
  name                                = "vpc"
  stage                               = "ec2-dev"
  map_public_ip_on_launch             = "true"
  total-nat-gateway-required          = "1"
  create_database_subnet_group        = "true"
  vpc-cidr                            = "10.20.0.0/16"
  vpc-public-subnet-cidr              = ["10.20.1.0/24","10.20.2.0/24"]
  vpc-private-subnet-cidr             = ["10.20.4.0/24","10.20.5.0/24"]
}


module "sg1" {
  source              = "../../modules/aws-sg-cidr"
  namespace           = "stocktest"
  stage               = "dev"
  name                = "ec2"
  tcp_ports           = "22,80,443"
  cidrs               = ["111.119.187.1/32"]
  security_group_name = "ecs"
  vpc_id              = module.vpc.vpc-id
}


module "ec2-stocktest-ec2" {
  source                        = "../../modules/aws-ec2"
  namespace                     = "stocktest.ca"
  stage                         = "dev"
  name                          = "stocktest-ec2"
  key_name                      = "ecs"
  public_key                    = file("../../modules/secrets/ecs.pub")
  user_data                     = file("../../modules/aws-ec2/user-data/user-data.sh")
  instance_count                = 1
  ami                           = "ami-00eb20669e0990cb4"
  instance_type                 = "t2.medium"
  associate_public_ip_address   = "true"
  root_volume_size              = 10
  subnet_ids                    = module.vpc.public-subnet-ids
  vpc_security_group_ids        = [module.sg1.aws_security_group_default]

}




module "alb-sg" {
  source              = "../../modules/aws-sg-cidr"
  namespace           = "stocktest.ca"
  stage               = "dev"
  name                = "ALB"
  tcp_ports           = "80,443"
  cidrs               = ["0.0.0.0/0"]
  security_group_name = "Application-LoadBalancer"
  vpc_id              = module.vpc.vpc-id
}

module "alb-ref" {
  source                  = "../../modules/aws-sg-ref-v2"
  namespace               = "stocktest"
  stage                   = "dev"
  name                    = "ALB-Ref"
  tcp_ports               = "80,443"
  ref_security_groups_ids = [module.alb-sg.aws_security_group_default,module.alb-sg.aws_security_group_default]
  security_group_name     = "ALB-Ref"
  vpc_id                  = module.vpc.vpc-id
}

module "alb-tg" {
  source = "../../modules/aws-alb-tg"
  #Application Load Balancer Target Group
  alb-tg-name               = "stocktest-tg"
  target-group-port         = "80"
  target-group-protocol     = "HTTP"
  vpc-id                    = module.vpc.vpc-id
}


module "alb" {
  source = "../../modules/aws-alb"
  alb-name                 = "stocktest-alb"
  internal                 = "false"
  alb-sg                   = module.alb-sg.aws_security_group_default
  alb-subnets              = module.vpc.public-subnet-ids
  alb-tag                  = "stocktest-alb"
  target-group-arn         = module.alb-tg.target-group-arn
}

